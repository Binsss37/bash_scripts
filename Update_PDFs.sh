#!/bin/bash
# Script which Convert all .odt and .txt files into pdf files (keep old pdf versions)
# test the -i option for "read"


date=`date '+%y%m%d'`



for fichier in `ls | grep -E '*.odt'`
do
    echo "Do you want to create a pdf version of $fichier ? (y or n)"
    read create_pdf
    
    if [ $create_pdf = "y" ]
    then
        base_name=`basename $fichier .odt`
    	echo "Do you want to delete the older pdf versions of $fichier ? (y or n)"
    	read delete_old_pdf

    	while [ $delete_old_pdf != "y" ] && [ $delete_old_pdf != "n" ]
        do
            echo "Please, answer by y (yes) or n (no). Do you want to delete the older pdf versions of $fichier ?"
            read delete_old_pdf
        done
    	
        if [ $delete_old_pdf = "y" ]
        then
            echo "Let's delete old files"
            rm $base_name\_??????.pdf
        
        elif [ $delete_old_pdf = "n" ]
        then
            echo "Keeping old pdf files for $fichier"
        fi
        
        unoconv $fichier
    	mv $base_name.pdf $base_name\_$date.pdf
    else
        echo "No pdf file created for $fichier"
    fi
done


for fichier in `ls | grep -E '*.txt'`
do
    echo "Do you want to create a pdf version of $fichier ? (y or n)"
    read create_pdf

    if [ $create_pdf = "y" ]
    then
        base_name=`basename $fichier .txt`
    	echo "Do you want to delete the older pdf versions of $fichier ? (y or n)"
    	read delete_old_pdf
    	
        while [ $delete_old_pdf != "y" ] && [ $delete_old_pdf != "n" ]
        do
            echo "Please, answer by y (yes) or n (no). Do you want to delete the older pdf versions of $fichier ?"
            read delete_old_pdf
        done
    	
        if [ $delete_old_pdf = "y" ]
        then
            echo "Let's delete old files"
            rm $base_name\_??????.pdf
        
        elif [ $delete_old_pdf = "n" ]
        then
            echo "Keeping old pdf files for $fichier"
        fi
    	
        unoconv $fichier
    	mv $base_name.pdf $base_name\_$date.pdf

    else
        echo "No pdf file created for $fichier"
    fi
done

# Convert all odt and txt files into pdf files
#unoconv *.odt
#unoconv *.txt

