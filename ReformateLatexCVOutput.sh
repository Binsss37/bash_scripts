#!/bin/bash

# Command ReformatePDF initial_file.pdf($1) output 

echo "In which language do you apply ?\n    e = english\n    f = français"
read language
while [ $language != "e" ] && [ $language != "f" ]
    do
        echo "select e (english) or f (français)"
        read language
    done
    if [ $language = "e" ]
    then
        CV="CV_BJoffres_"
        LM="CL_BJoffres_"
        reco="Recommendation_letters_BJoffres_"
    elif [ $language = "f" ]
    then
        CV="CV_BJoffres_"
        LM="LM_BJoffres_"
        reco="Lettres_recommandation_BJoffres_"
    fi


echo "What is the position title ?"
read position


echo "How many pages contains the CV ?"
read pages

CV_name=$CV$position.pdf
LM_name=$LM$position.pdf
reco_name=$reco$position.pdf

pdftk $@ burst


if [ $pages = 1 ]
then
	mv pg_0001.pdf $CV$position.pdf

    mv pg_0002.pdf $LM$position.pdf

    pdftk pg_0003.pdf pg_0004.pdf pg_0005.pdf pg_0006.pdf pg_0007.pdf cat output $reco$position.pdf
    rm pg_0003.pdf pg_0004.pdf pg_0005.pdf pg_0006.pdf pg_0007.pdf

elif [ $pages = 2 ]
then
	pdftk pg_0001.pdf pg_0002.pdf cat output $CV$position.pdf
    rm pg_0001.pdf pg_0002.pdf

    mv pg_0003.pdf $LM$position.pdf

    pdftk pg_0004.pdf pg_0005.pdf pg_0006.pdf pg_0007.pdf pg_0008.pdf cat output $reco$position.pdf
    rm pg_0004.pdf pg_0005.pdf pg_0006.pdf pg_0007.pdf pg_0008.pdf
fi


echo "How do you want to include the recommendation letters ?
     1 - with the CV
     2 - with the cover letter
     3 - in a sperate file"
read reco_option


if [ $reco_option = 1 ]
then
    pdftk $CV_name $reco_name cat output fleeting_file.pdf
    rm $CV_name $reco_name
    mv fleeting_file.pdf $CV_name 
elif [ $pages = 2 ]
then
    pdftk $LM_name $reco_name cat output fleeting_file.pdf
    rm $LM_name $reco_name
    mv fleeting_file.pdf $LM_name 
fi






